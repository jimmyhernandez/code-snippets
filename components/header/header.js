"use strict";
use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
     "/libs/wcm/foundation/components/utils/Image.js",
     "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, Image, Q) {


	
    var header = {
    	courtesyNav:granite.resource.properties["courtesyNav"] || "top",
    	mainNav:granite.resource.properties["mainNav"] || "bottom",
    	search:granite.resource.properties["search"] || "beside",
    };

    return header;
});