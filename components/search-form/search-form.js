/* global use:false, currentPage: false, properties: false, inheritedPageProperties: false, request: false */
/**
 * Copyright 2016 Integration New Media Inc.
 * Search Form component JS backing script
 */

use(function () {
    "use strict";
    var CONST = {
            SEARCH_PAGE_PROPERTY_NAME: "searchPage",
            DEFAULT_COMPONENT_ID: "searchfield",
            MOBILE_SUFFIXE: "_mobile"
        },
        result = {},
        resultPagePath,
        selectors = request.getRequestPathInfo().getSelectorString();

    resultPagePath = inheritedPageProperties.get(CONST.SEARCH_PAGE_PROPERTY_NAME, "");
    result.componentId = CONST.DEFAULT_COMPONENT_ID;
    result.test = resultPagePath;
    if (selectors && selectors.indexOf("mobile") > -1) {
        result.componentId += CONST.MOBILE_SUFFIXE;
    }
    result.searchPagePath = resultPagePath;
    result.CONST = CONST;
    return result;
});