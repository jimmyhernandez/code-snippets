"use strict";
use(function () {

	var CONST = {

        PROP_SLIDESHOW_ALIGN_TEXT: "alignText",
        PROP_SLIDESHOW_ASPECT_RATIO: "aspectRatio",
        PROP_SLIDESHOW_NAVIGATION: "navigation",

        CHILD_NODE_SLIDE_DATAS:"slideDatas",

        PROP_SLIDE_TITLE: "title",
        PROP_SLIDE_SUBTITLE: "subtitle",
        PROP_SLIDE_LINK_TO: "linkTo",
        PROP_SLIDE_LINK_TARGET: "openLinkInNewTab",
        PROP_SLIDE_FILE_NAME: "fileName",
        PROP_SLIDE_FILE_REF: "fileRef",

    };

	var contentPath = currentNode.getIdentifier();
    var arrContentpath = contentPath.split("/");
    var id = (arrContentpath ? arrContentpath[arrContentpath.length-1] : "");
	
    var navigation = currentNode.hasProperty(CONST.PROP_SLIDESHOW_NAVIGATION) ? currentNode.getProperty(CONST.PROP_SLIDESHOW_NAVIGATION).toString() : "default";
    var arraySlides = [];

    if(currentNode.hasNode("slideDatas")){
		var slideDatas = currentNode.getNode("slideDatas")
        var lstSlidesNode = slideDatas.getNodes();
    
    
        for(var i=0; i<lstSlidesNode.length; i++){
            var colNode = lstSlidesNode[i];
            var objCols = {
                title:		colNode.hasProperty(CONST.PROP_SLIDE_TITLE) ? colNode.getProperty(CONST.PROP_SLIDE_TITLE).toString() : "",
                subtitle:	colNode.hasProperty(CONST.PROP_SLIDE_SUBTITLE) ? colNode.getProperty(CONST.PROP_SLIDE_SUBTITLE).toString() : "",
                linkTo:		colNode.hasProperty(CONST.PROP_SLIDE_LINK_TO) ? adjustLinkPath(colNode.getProperty(CONST.PROP_SLIDE_LINK_TO).toString()) : "",
                linkTarget:	colNode.hasProperty(CONST.PROP_SLIDE_LINK_TARGET) && colNode.getProperty(CONST.PROP_SLIDE_LINK_TARGET).toString() == "on" ? "_blank" : "_self",
                fileName:	colNode.hasProperty(CONST.PROP_SLIDE_FILE_NAME) ? colNode.getProperty(CONST.PROP_SLIDE_FILE_NAME).toString() : "",
                fileRef:	colNode.hasProperty(CONST.PROP_SLIDE_FILE_REF) ? colNode.getProperty(CONST.PROP_SLIDE_FILE_REF).toString() : "",
                image:		colNode.hasProperty(CONST.PROP_SLIDE_FILE_REF) ? colNode.getProperty(CONST.PROP_SLIDE_FILE_REF).toString() : ""
            };
            arraySlides.push(objCols);
        }
    }else{
		var jsonValue = {"title":"Slide Title","subtitle":"Slide Subtitle"};
        arraySlides.push(jsonValue)
    }


	// Add .html to link path if necessary
	function adjustLinkPath(path){
        var newpath = path;

		if(path.indexOf("http") > -1 || path.indexOf(".html") > -1 )
			newpath = path;
       else
           newpath = path+".html";

        return newpath;
    }

    // Return Slideshow Datas
    return {
        id:id,
        navigation:navigation,

        arrows: navigation == "arrows_dots" || navigation == "arrows" || navigation == "default" ? true : false,
    	dots: navigation == "arrows_dots" || navigation == "dots" || navigation == "default" ? true : false,
		infinite: true,
		slidesToShow: granite.resource.properties["slidesToShow"] || 1,
		slidesToScroll: granite.resource.properties["slidesToScroll"] || 1,
		centerMode: granite.resource.properties["centerMode"] || false,
		autoplay: granite.resource.properties["autoplay"] || false,
		viewer: granite.resource.properties["viewer"] || false,
		
		nbSlides:arraySlides.length,
        arraySlides:arraySlides,
    };

});