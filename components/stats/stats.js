"use strict";

use(function () {
    var stats = {
        data: []
    };
    var colsData = currentNode.hasNode("colsData");

    if(colsData) {
        var nodeColsData = currentNode.getNode("colsData");
        var lstColsDataNodes = nodeColsData.getNodes();

        for(var i = 0; i < lstColsDataNodes.length; i++) {
            var colNode = lstColsDataNodes[i];
            var statNode = {
                number: colNode.getProperty("number").toString(),
                text: colNode.getProperty("text").toString()
            };

            stats.data.push(statNode);

        }

    }

    stats.info = "Test";

    return stats;
});