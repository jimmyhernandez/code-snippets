"use strict";
use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
     "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, Video, Q) {
	
	var CONST = {
	        PROP_SLIDESHOW_ASPECT_RATIO: "aspectRatio",
	        PROP_SLIDE_FILE_NAME: "fileName",
	        PROP_SLIDE_FILE_REF: "fileReference",
	    };	
	
	
	var src = currentNode.hasProperty(CONST.PROP_SLIDE_FILE_REF) ? currentNode.getProperty(CONST.PROP_SLIDE_FILE_REF).toString() : "";
    var fileName = currentNode.hasProperty(CONST.PROP_SLIDE_FILE_NAME) ? currentNode.getProperty(CONST.PROP_SLIDE_FILE_NAME).toString() : "";
    if(src=="" && fileName != ""){
		src = currentNode.getPath() + "/file";
    }
    
	//var type = (src.indexOf("content/") > -1 ? "dam" : "external");
    var type = "";
    
    if(src.indexOf("youtubeID:") > -1){
    	var res = src.split("youtubeID:");
    	src = "//www.youtube.com/embed/"+res[1]+"?rel=0&showinfo=0&enablejsapi=1";
    	type = "external";
    }else if(src.indexOf("dailymotionID:") > -1){
    	var res = src.split("dailymotionID:");
    	src = "//www.dailymotion.com/embed/video/"+res[1];
    	type = "external";
    }else{
    	type = "dam";
    }
    
	var aspectRation = currentNode.hasProperty(CONST.PROP_SLIDESHOW_ASPECT_RATIO) ? currentNode.getProperty(CONST.PROP_SLIDESHOW_ASPECT_RATIO).toString() : "16-9";
	var mimetype = "video/mp4";
	
	var video = {
		aspectRation:	aspectRation,
        type:			type,
        src:			src,
		mimetype:		mimetype
	};
	
	return video;
})