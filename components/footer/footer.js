"use strict";

/**
 * Copyright 2016 Integration New Media Inc.
 * Footer component JS backing script
 */

use(["wcm/foundation/components/utils/AuthoringUtils.js",
     "wcm/foundation/components/utils/ResourceUtils.js"], function (AuthoringUtils, ResourceUtils) {
    var CONST = {
        PROP_FOOTER_COLS: "footercols",
        BOOTSTRAP_XS_COLUMNS: 12,
        PAR_TEXT: 'parsys_footer_'
    };

    var footer = {};

    // createCols is called from within the getCols below
    var createCols = function (cols) {
        var CONST = {
            PROP_FOOTER_COLS: "footercols",
            BOOTSTRAP_XS_COLUMNS: 12,
            PAR_TEXT: 'parsys_footer_'
        };
        // footer column logic based on design dialog cols option
        footer.options = [];
        footer.cols = cols;
        var lg = 0;
        var md = 0;
        var xs = 0;
        var sm = 0;
        for (var i=1; i<=footer.cols; i++) {
            if(footer.cols === 4) {
                sm = 6;
            } else {
                sm = CONST.BOOTSTRAP_XS_COLUMNS;                
            }
            lg = CONST.BOOTSTRAP_XS_COLUMNS / footer.cols;
            md = CONST.BOOTSTRAP_XS_COLUMNS / footer.cols;
            xs = CONST.BOOTSTRAP_XS_COLUMNS;    
            footer.options.push({
                lg: lg,
                md: md,
                sm: sm, 
                xs: xs,
                path: CONST.PAR_TEXT + i
            });
        }

        // Adding the constants to the exposed API
        footer.CONST = CONST;
        return footer;
    };
    
    // Get the footer cols based on author entry
    var getCols =  properties.get(CONST.PROP_FOOTER_COLS, "1");
    var footer = {};
    var cols = parseInt(getCols, 10);
    footer.options = [];
    footer = createCols(cols);
    return footer;

});
