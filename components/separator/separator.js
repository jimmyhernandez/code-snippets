"use strict";
use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
     "/libs/wcm/foundation/components/utils/Image.js",
     "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, Image, Q) {
	
    var separator = {
    	style:granite.resource.properties["style"] || "default",
    	color: granite.resource.properties["color"] || "#666",
    	height: (granite.resource.properties["height"] && granite.resource.properties["height"] > 0 ? granite.resource.properties["height"] : 1),
    	line: granite.resource.properties["line"] || "solid",
        paddingTop:granite.resource.properties["padding-top"] || 0,
        paddingBottom:granite.resource.properties["padding-bottom"] || 0,
        paddingLeft:granite.resource.properties["padding-left"] || 0,
        paddingRight:granite.resource.properties["padding-right"] || 0,
    };

    
    return separator;
});