/**
 * Copyright 2016 Integration New Media Inc.
 * Breadcrumb component JS backing script

 * This is the breadcrumb controller, it return the list of links that we should display in
 * the breadcrumb.
 */

use(function () {
    var CONST = {
        LEVEL : 2,
        END_LEVEL : 0,
        DELIMITER : '/',
        TRAILING : '' 
    };
    
    var _getBreadcrumbComponents = function () {
        var currentLevel = currentPage.getDepth();
        var level = currentStyle.get("absParent") || CONST.LEVEL;
        var endLevel = currentStyle.get("relParent") || CONST.END_LEVEL;
        var result = [];
        while (level < currentLevel - endLevel) {
            var trail = currentPage.getAbsoluteParent(level);
            if (!trail) {
                break;
            }
            var title = trail.getNavigationTitle();
            if (!title || title ==="") {
                title = trail.getTitle();
            }
            if (!title || title ==="") {
                title = trail.getName();
            }
            result.push({
                href : xssAPI.getValidHref(trail.getPath()+'.html'),
                text : title
            });
            level ++;
        }
        if ( result.length > 0 ) {
            result[ result.length-1 ].class = "selected";
            result[ result.length-1].isCurrent = true;
        }
        return result;
    };
    
    return {
        CONST : CONST,
        components : _getBreadcrumbComponents()
    };
});
