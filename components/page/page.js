"use strict";

/**
 * Copyright 2016 Integration New Media Inc.
 * base page component JS backing script
 */
use(function () {

    var page = {};
    page.root = currentPage.getAbsoluteParent(2).getPath();

    return page;

});
