$(document).ready(function(){
	
	$('.login-dropdown .dropdown-toogle').on('click', function (event) {
	    $(this).closest(".login-dropdown").toggleClass('open');
	});
	$('body').on('click', function (e) {
	    if (!$('.login-dropdown').is(e.target) 
	        && $('.login-dropdown').has(e.target).length === 0 
	        && $('.open').has(e.target).length === 0
	    ) {
	        $('.login-dropdown').removeClass('open');
	    }
	});

	$(".swicth-tab").click(function(event){
		event.preventDefault();
		var target = $(this).attr("data-target");
		$(".form-tab").removeClass("active");
		$(".form-tab#"+target).addClass("active");
	});
	
})