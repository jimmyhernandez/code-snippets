package apps.inm_starterkit.components.content.login;
  
import com.adobe.cq.sightly.WCMUsePojo;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataOutputStream;

import java.util.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.net.URLDecoder;

import java.lang.StringBuilder;

import javax.jcr.Session;

import org.apache.commons.lang.ArrayUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.JSONException;

import com.day.cq.wcm.webservicesupport.ConfigurationManager;
import com.day.cq.wcm.webservicesupport.Configuration;


import java.security.MessageDigest;


import com.inm.aem.accelerators.starterkit.core.MailchimpUtils;
import com.inm.aem.accelerators.starterkit.core.models.MailchimpInterest;
import com.inm.aem.accelerators.starterkit.core.models.MailchimpInterestCategory;
import com.inm.aem.accelerators.starterkit.core.models.MailchimpMergeFields;
import com.inm.aem.accelerators.starterkit.core.models.MailchimpList;
import com.inm.aem.accelerators.starterkit.core.models.MailchimpMember;

public class Login extends WCMUsePojo {

	private String rootUrl;
	
	private String userID;
	private String email;
	private String firstName;
	private String lastName;

    @Override
    public void activate() throws Exception {
    	
    	rootUrl = getCurrentPage().getAbsoluteParent(2).getPath();
    	
        /*-Get Current AEM User Id-*/
    	Session session = getResourceResolver().adaptTo(Session.class);
    	userID = session.getUserID();
		UserManager userManager = getResourceResolver().adaptTo(UserManager.class);
		Authorizable auth = userManager.getAuthorizable(userID);

		/*-Get Current AEM User Infos-*/
		email = ((auth.getProperty("./profile/email") != null && auth.getProperty("./profile/email").length > 0) ? auth.getProperty("./profile/email")[0].getString() : "");
		firstName = ((auth.getProperty("./profile/givenName") != null && auth.getProperty("./profile/givenName").length > 0) ? auth.getProperty("./profile/givenName")[0].getString() : "");
		lastName = ((auth.getProperty("./profile/familyName") != null && auth.getProperty("./profile/familyName").length > 0) ? auth.getProperty("./profile/familyName")[0].getString() : "");
	
	}
    public String getRootUrl(){
    	return rootUrl;
    }
    
    public String getUserID(){
    	return userID;
    }
    public String getEmail(){
    	return email;
    }
    public String getFirstName(){
    	return firstName;
    }
    public String getLastName(){
    	return lastName;
    }

}