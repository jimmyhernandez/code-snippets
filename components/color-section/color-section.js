"use strict";
use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/Image.js",
    "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, Image, Q) {

    /*-Image-*/
    var image = new Image(granite.resource);
    var imageDefer = Q.defer();

    // check if there's a local file image under the node
    granite.resource.resolve(granite.resource.path + "/file").then(function (localImageResource) {
        imageDefer.resolve(image);
    }, function() {
        imageDefer.resolve(image);
    });

    /*-Padding-*/
    var paddingTop = granite.resource.properties["padding-top"] || 0;
    var paddingBottom = granite.resource.properties["padding-bottom"] || 0;
    var paddingLeft = granite.resource.properties["padding-left"] || 0;
    var paddingRight = granite.resource.properties["padding-right"] || 0;
    var customPadding = "padding:"+paddingTop+"px "+paddingRight+"px "+paddingBottom+"px "+paddingLeft+"px;";
    var fileName = granite.resource.properties["fileName"];
    var fileReference = granite.resource.properties["fileReference"];

    var colorSection = {
        image:(fileName || fileReference ? imageDefer.promise : null),
        backgroundColor: granite.resource.properties["backgroundColor"] || "#fff",
        textColor:granite.resource.properties["textColor"] || "#000",
        padding:granite.resource.properties["padding"] || "small",
        customPadding:customPadding,
        paddingTop:granite.resource.properties["paddingTop"] || "",
        paddingBottom:granite.resource.properties["paddingBottom"] || "",
        paddingLeft:granite.resource.properties["paddingLeft"] || "",
        paddingRight:granite.resource.properties["paddingRight"] || "",
    };

    return colorSection;
});