/**
 * global use: false, currentPage: false
 */

use(function () {
    var pageParam = this.pageParam;
    var _isCurrentPageInPath = function () {
        if (!pageParam) {
            return false;
        }
        var pagePath = pageParam.path.replace('html', '');
        var currentPath = currentPage.path.replace('html', '');
        return (currentPath.indexOf(pagePath) > -1) && (currentPage.depth != pageParam.depth);
    };
    
    return {
        isCurrentPageInPath: _isCurrentPageInPath(),
        param: pageParam
    };
});
