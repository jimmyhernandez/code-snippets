/*global currentStyle: false, currentPage: false */
/**
 * Copyright 2016 Integration New Media Inc.
 * Sidebar Navigation component JS backing script
 */

use(function () {
    var CONST = {
        LEVEL: 2
    };

    /**
     * For now we are not using any configuration dialog, if we need to
     * We can make the level configurable.
     */
    var level = CONST.LEVEL;
    return {
        root: currentPage.getAbsoluteParent(parseInt(level, 10)),
        level: level
    };
});