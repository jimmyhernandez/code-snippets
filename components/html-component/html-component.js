"use strict";

/**
 * Copyright 2016 Integration New Media Inc.
 * HTML Component component JS backing script
 */

use(["wcm/foundation/components/utils/AuthoringUtils.js"], function (AuthoringUtils) {
    
    var CONST = {
        PROP_ALERT_TYPE: "type",
        PROP_ALERT_TEXT: "text",
        PROP_HTML_CODE: "htmlCode"
    }

    var htmlComponent = {};
    
    htmlComponent.wcmmode = wcmmode.toString();
    htmlComponent.htmlCode = granite.resource.properties[CONST.PROP_HTML_CODE]
    
    // Set a placeholder for author environment
    if (!htmlComponent.htmlCode && htmlComponent.wcmmode == "EDIT") {
        htmlComponent.cssClass = AuthoringUtils.isTouch
                ? "cq-placeholder section"
                : "cq-text-placeholder-ipe";
        htmlComponent.rowclass = ""
    } else {
        htmlComponent.rowclass = "row";
    }
    
    // Adding the constants to the exposed API
    htmlComponent.CONST = CONST;
    
    return htmlComponent;
    
});