"use strict";
/**
 * Copyright 2016 Integration New Media Inc.
 * Column Control component JS backing script
 */
use(function () {
    var hasNodeColsData = currentNode.hasNode("colsData");
    var arrCols = [];
    if(hasNodeColsData) {
        var nodeColsData = currentNode.getNode("colsData");
        var lstColsDataNodes = nodeColsData.getNodes();


        for (var i = 0; i < lstColsDataNodes.length; i++) {
            var colNode = lstColsDataNodes[i];
            var objCols = {
                classLG: "col-lg-" + colNode.getProperty("largeScreen").toString(),
                classMD: "col-md-" + colNode.getProperty("medScreen").toString(),
                classSM: "col-sm-" + colNode.getProperty("smallScreen").toString()
            };
            arrCols.push(objCols);
        }

        return {
            listCols: arrCols
        };
    } else {
        var objCols = {
            classLG: "col-lg-6",
            classMD: "col-md-6",
            classSM: "col-sm-12"
        };
        arrCols.push(objCols);
        arrCols.push(objCols);
        return { listCols: arrCols}
    }
});