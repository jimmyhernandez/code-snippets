/*global currentStyle: false, currentPage: false, use: false */
/**
 * Copyright 2016 Integration New Media Inc.
 * Lang Selector JS backing script
 */
use(function () {
    var CONST = {
        FRENCH: "fr",
        ENGLISH: "en",
        FRENCH_LONG: "Français",
        ENGLISH_LONG: "English"
    };
    
    /**
     * Here is an example how the JSON looks like when extracting the page languages:
     * {
     *      "en":{
     *          "path":"/content/starterkit/en/about-inm",
     *          "exists":true,
     *          "hasContent":true,
     *          "lastModified":0,
     *          "iso":"en",
     *          "country":"",
     *          "language":"English"
     *      },
     *      "fr":{
     *          "path":"/content/starterkit/fr/about-inm",
     *          "exists":true,
     *          "hasContent":true,
     *          "lastModified":0,
     *          "iso":"fr",
     *          "country":"",
     *          "language":"French"
     *     }
     * }
     */
    var lang = "" + currentPage.getLanguage(true).getLanguage().toString(),
        pageLanguages = JSON.parse(this.pageLangUtils.pageLanguages),
        translationPath = "",
        displayWarning = true,
        langName = CONST.ENGLISH_LONG,
        ariaLabel = "";

    if (lang === CONST.FRENCH) {
        if (pageLanguages.en && pageLanguages.en.exists) {
            lang = CONST.ENGLISH;
            translationPath = pageLanguages.en.path;
            displayWarning = false;
            langName = CONST.ENGLISH_LONG;
            ariaLabel = 'Open this page in english';
        }
    } else if (lang === CONST.ENGLISH) {
        if (pageLanguages.fr && pageLanguages.fr.exists) {
            lang = CONST.FRENCH;
            translationPath = pageLanguages.fr.path;
            displayWarning = false;
            langName = CONST.FRENCH_LONG;
            ariaLabel = 'Ouvrir cette page en français';
        }
    } else {
        lang = CONST.ENGLISH;
        translationPath = currentPage.path;
        langName = CONST.ENGLISH_LONG;
        ariaLabel = 'Open this page in english';
    }
    
    return {
        lang: lang,
        langName: langName,
        path: translationPath,
        warn: displayWarning,
        ariaLabel: ariaLabel
    };
});
