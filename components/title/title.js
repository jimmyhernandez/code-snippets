"use strict";

/**
 * Copyright 2016 Integration New Media Inc.
 * Title Sightly component JS backing script
 */
use(function () {

    var CONST = {
        PROP_TITLE: "jcr:title",
        PROP_PAGE_TITLE: "pageTitle",
        PROP_TYPE: "type",
        PROP_DEFAULT_TYPE: "defaultType",
        PROP_ALIGN: "align",
        PROP_HIDDEN: "hidden"
    }
    
    var title = {};
    title.sr_only ="";
    title.hidden = "";
    var type = granite.resource.properties[CONST.PROP_TYPE];

    // The actual title content
    title.text = granite.resource.properties[CONST.PROP_TITLE]
            || wcm.currentPage.properties[CONST.PROP_PAGE_TITLE]
            || wcm.currentPage.properties[CONST.PROP_TITLE]
            || wcm.currentPage.name;
    
    if(type == CONST.PROP_HIDDEN) {
        title.element = currentStyle.get(CONST.PROP_DEFAULT_TYPE, "");
        if(wcmmode.toString().equals("EDIT")) {
           title.hidden = "{hidden}";
        } else { 
            title.sr_only = "sr-only";
            
        }
    } else {
    
        // The HTML element name
        title.element = granite.resource.properties[CONST.PROP_TYPE]
                || currentStyle.get(CONST.PROP_DEFAULT_TYPE, "");
    }

    // Get the alignment property

    title.align = granite.resource.properties[CONST.PROP_ALIGN];   
    
    // Adding the constants to the exposed API
    title.CONST = CONST;
    
    return title;
    
});
