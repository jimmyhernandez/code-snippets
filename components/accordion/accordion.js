"use strict";

/**
 * Copyright 2016 Integration New Media Inc.
 * Accordion component JS backing script
 */
use(function () {

    var CONST = {
        PROP_TITLE: "title",
        PROP_ID: "id",
        PROP_STATE: "state",
        PROP_STYLE: "style",
    };

    var accordion = {};

    // The actual content
    accordion.title = granite.resource.properties[CONST.PROP_TITLE];
    accordion.id = granite.resource.properties[CONST.PROP_ID];
    accordion.state = granite.resource.properties[CONST.PROP_STATE];
    accordion.style = granite.resource.properties[CONST.PROP_STYLE];
    accordion.wcmmode = wcmmode.edit;
    accordion.in = "";
    accordion.intitle = "";
    accordion.titleClass = "";
    accordion.warningMsg = "";
    accordion.expanded = "false";
    accordion.arialabel = "Collapse Button";

    accordion.uid = currentNode.getIdentifier();
    // split the identifier slashes
    var cleanid = accordion.uid.replace(':','_');
    var identifier = cleanid.split('/');
    var id = identifier[identifier.length - 1];
    var uid = identifier[identifier.length - 2] + "_" + identifier[identifier.length - 1];
    accordion.uid = uid;

    // Expand accordion if author has set state to open 
    if (accordion.state == "Open") {
        accordion.expanded = "true"
        accordion.arialabel = "Collapse Button";
        accordion.in = "in"
        accordion.intitle = "in-title"
    } else {
        accordion.in = "collapsed"
        accordion.intitle = "collapsed"
        accordion.arialabel = "Expand Button";
    }

    // Expand accordion content parsys in edit mode
    if (wcmmode.edit == true) {
        accordion.in = "in"
    }

    // Set title class if title is undefined 
    if (accordion.title == undefined) {
        accordion.titleClass = "text-danger"
    }

    // Adding the constants to the exposed API
    accordion.CONST = CONST;

    return accordion;

});
