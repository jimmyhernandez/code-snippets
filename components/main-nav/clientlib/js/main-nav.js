/*jslint browser: true*/
/*global $*/

$(document).ready(function () {
    'use strict';
    var $mainMenu = '';

    $mainMenu = $('#main-nav').on('click', 'button.sub-arrow', function (e) {
            // toggle the sub menu on sub arrow click in collapsible mode
            var obj = $mainMenu.data('smartmenus'),
                $item = $(this).next(),
                $sub = $item.parent().dataSM('sub'),
                subIsVisible = $sub.dataSM('shown-before') && $sub.is(':visible');
            if (obj.isCollapsible()) {
                $sub.dataSM('arrowClicked', true);
                obj.itemActivate($item);
                if (subIsVisible) {
                    obj.menuHide($sub);
                }
                e.stopPropagation();
                e.preventDefault();
            }
        }).bind({
            // don't show the sub menu in collapsible mode unless the sub arrow is clicked
            'beforeshow.smapi': function (e, menu) {
                var obj = $mainMenu.data('smartmenus'),
                    $menu = $(menu);
                if (obj.isCollapsible()) {
                    if (!$menu.dataSM('arrowClicked')) {
                        return false;
                    }
                    $menu.removeDataSM('arrowClicked');
                }
            },
            'show.smapi': function (e, menu) {                
                //$(menu).dataSM('parent-a').children('span.sub-arrow').text('-');
                var parentWidth = $(menu).parent().innerWidth();
                var menuWidth = $(menu).innerWidth();
                var margin = (parentWidth / 2) - (menuWidth / 2);
                margin = margin + "px";
                $(menu).css("margin-left", margin);
            },
            'hide.smapi': function (e, menu) {
                //$(menu).dataSM('parent-a').children('span.sub-arrow').text('+');
            },
            
        });
    
        // Change default menu expand button for accessibility, follows Bootstrap's attributes and behaviour
        $('.starterkit-main-nav .sub-arrow').each(function() {
            $(this).empty();
            var text =   $(this).parent().text();
            var textForHTML = $ESAPI.encoder().encodeForHTML(text);
            var textForHTMLAttr = $ESAPI.encoder().encodeForHTMLAttribute(text);

            var expanded = 'false';
            if ($(this).parent().hasClass("highlighted")) {
                var expanded = 'true';
            }
            var parentli = $(this).parent().parent();
            $(parentli)
                .prepend(
                     $(this)
                );
            $(this).replaceWith( "<button class='sub-arrow' aria-label='" + textForHTMLAttr + "' aria-controls='" + textForHTMLAttr + "' aria-expanded='" + expanded + "'>" + textForHTML + "</button>" );
            
        });
    
        $('.starterkit-main-nav .sub-arrow').click(function() {
            var expanded = 'false';
            if ($(this).attr('aria-expanded') == expanded) {
                $(this).attr("aria-expanded", 'true');
            } else {
                $(this).attr("aria-expanded", expanded);
            }
        });
        
        
    
        // mobile navigation & search handling
        $('#search-form-mobile-collapse').collapse({
            toggle: false
        });
        $('#main-nav-collapse').collapse({
            toggle: false
        });
    
        $( ".main-nav-toggle" ).click(function() {
            $('#search-form-mobile-collapse').collapse('hide');
            $('#main-nav-collapse').collapse('show');
            if ($('#main-nav-collapse').hasClass("in")) {
                $('#main-nav-collapse').collapse('hide');
            }
        });
        $( ".search-toggle" ).click(function() {
            $('#main-nav-collapse').collapse('hide');
            $('#search-form-mobile-collapse').collapse('show');
            if ($("search-form-mobile-collapse").hasClass("in")) {
                $("search-form-mobile-collapse").collapse('hide');
            }
        });
});
