/*global currentStyle: false, currentPage: false */
/**
 * Copyright 2016 Integration New Media Inc.
 * Main Nav component JS backing script
 */

use(function () { 
    var CONST = {
        LEVEL : 2
    };
    var level = currentStyle.get("absParent") || CONST.LEVEL;
    return {
	    root: currentPage.getAbsoluteParent(parseInt(level, 10)),
        level: currentStyle.get("absParent")
    };
});
